
Quickfile module for Drupal 5.0.x

By Alexis Bellido <alexis@ventanazul.com>

Description
-----------
The Quickfile module allows instant selling and download of digital products such as ebooks, photographs, music and software. Clients don't need to be registered to buy and they get an url to start downloading right after making their payment.

The file module in the Ecommerce package is quite helpful and I'm thankful for many ideas and code I got from from there, but I've noticed that in many sites clients get a little lost when they need to register a Drupal account, wait for a confirmation email and then login and go to "My Files" page to get their files. This can reduce the sales of ebooks.

The Quickfile module creates a new node type called 'quickfile'.

With the Quickfile module clients just need to visit three web pages in a single session to make the payment and download a file.  

Features:
---------
* Clients can buy and download any digital product without a Drupal account.
* Supports Authorize.net and Paypal with their test modes.
* Use multipage form approach for showing confirmation page.
* Discount coupons supporting fixed or percentage discounts and date validity.
* Coupons can be applied to one ebook or for all of them.
* Uses module jscalendar from Javascript Tools if installed. See: http://drupal.org/node/57285 
* Manual creation of transactions.
* Editing transactions allows to re-activate downloads.
* Protected downloads.
* Custom notification emails for multiple administrators and client.

Todo List:
----------
* Plugin system for other payment processors.
* Prefill forms with email of registered users if needed.
* Multiple select for coupons.
* Avoid duplicate coupon codes.
* Cross-selling: Buy X with Y and pay less.
* Search transactions and coupons.
* Separate coupons management in other module.
* Add more themeable functions.
* Support for other DBMS besides MySQL
* A shopping cart for buying several files in one transaction?
* AJAX support for showing payment form in file page like in http://gettingreal.37signals.com/ 
* Support for cron notifications to avoid lots of emails if many sales.

If you use this module and find it useful please let me know by visiting:

In English: http://www.ventanazul.com/webzine/articles/ebook-module-drupal

En espa�ol: http://www.ventanazul.com/articulos/ebook-module-drupal

or writing to alexis@ventanazul.com.

I'm also available for paid Drupal work, so let's have a chat, I'll be glad
to help.
